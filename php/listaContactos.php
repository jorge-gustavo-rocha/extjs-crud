<?php
//chama o arquivo de conexão com o bd
include ("db.php");

if (isset($_REQUEST['start']) && strlen(trim($_REQUEST['start'])) > 0) {
	$start = $_REQUEST['start'];
} else {
	$start = 0;
}
if (isset($_REQUEST['limit']) && strlen(trim($_REQUEST['limit'])) > 0) {
	$limit = $_REQUEST['limit'];
} else {
	$limit = 25;
}
if ($limit > 50) {
	$limit = 50;
}

$queryConta = "select count(*) from contacto ";
$res = &$mdb2 -> query($queryConta);
if (PEAR::isError($res)) {
	die($res -> getMessage());
}
$row = $res -> fetchRow(MDB2_FETCHMODE_ASSOC);
$total = $row['count'];

$sql  = "select id, nome, telefone, email from contacto ";
$sql .= "LIMIT " . $limit . " OFFSET " . $start;
$res = &$mdb2 -> query($sql);
if (PEAR::isError($res)) {
	die($res -> getMessage());
}

$tabela = array();
while ($row = $res -> fetchRow(MDB2_FETCHMODE_ASSOC)) {
	array_push($tabela, $row);
}
$resposta["contactos"] = $tabela;
$resposta["success"] = true;
$resposta["total"] = $total;

header('Content-type: application/json');
echo json_encode($resposta);
$mdb2 -> disconnect();
?>