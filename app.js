Ext.require([ 'Ext.grid.*', 'Ext.data.*', 'Ext.panel.*',
		'Ext.layout.container.Border' ]);
Ext.onReady(function() {
	Ext.define('Contacto', {
		extend : 'Ext.data.Model',
		fields : [ 'id', 'nome', 'telefone', 'email' ]
	});
	var store = Ext.create('Ext.data.Store', {
		model : 'Contacto',
		autoSync : true, // atualiza na BD sempre que sofrer alterações...
		autoLoad : true,
		pageSize : 35,
		autoLoad : {
			start : 0,
			limit : 35
		},
		proxy : {
			type : 'ajax',
			api : {
				create : 'php/criaContacto.php',
				read : 'php/listaContactos.php',
				update : 'php/atualizaContacto.php',
				destroy : 'php/removeContacto.php',
			},
			reader : {
				type : 'json',
				root : 'contactos',
				successProperty : 'success'
			},
			writer : {
				type : 'json',
				writeAllFields : false,
				encode : true,
				root : 'contactos'
			}
		}
	});
	var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
		listeners : {
			cancelEdit : function(rowEditing, context) {
				// Canceling editing of a locally added, unsaved record: remove
				// it
				if (context.record.phantom) {
					store.remove(context.record);
				}
			}
		}
	});
	var grid = Ext.create('Ext.grid.Panel', {
		store : store,
		title : 'Contactos',
		columns : [ {
			text : "Id",
			width : 40,
			dataIndex : 'id'
		}, {
			text : "Nome",
			flex : 1,
			dataIndex : 'nome',
			sortable : true,
			field : {
				xtype : 'textfield'
			}
		}, {
			text : "Telefone",
			width : 115,
			dataIndex : 'telefone',
			sortable : true,
			field : {
				xtype : 'textfield'
			}
		}, {
			text : "Email",
			width : 120,
			dataIndex : 'email',
			sortable : true,
			field : {
				xtype : 'textfield'
			}
		} ],
		viewConfig : {
			forceFit : true
		},
		region : 'center',
		tbar : [ {
			text : 'Novo contacto',
			handler : function() {
				rowEditing.cancelEdit();
				// Create a model instance
				var r = Ext.create('Contacto', {
					nome : 'Fulano',
					telefone : '987654321',
					email : 'fulano@sapo.pt'
				});
				store.insert(0, r);
				rowEditing.startEdit(0, 0);
			}
		}, {
			itemId : 'apagaContacto',
			text : 'Elimina contacto',
			handler : function() {
				var sm = grid.getSelectionModel();
				rowEditing.cancelEdit();
				store.remove(sm.getSelection());
				if (store.getCount() > 0) {
					sm.select(0);
				}
			},
			disabled : true
		} ],
		plugins : [ rowEditing ],
		listeners : {
			'selectionchange' : function(view, records) {
				grid.down('#apagaContacto').setDisabled(!records.length);
			}
		}
	});
	Ext.create('Ext.container.Viewport', {
		frame : true,
		layout : 'border',
		items : [ grid ]
	});
});
